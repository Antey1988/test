package com.example.table.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Setter@Getter
public class Params {
    private Filter filter;
    private Order order;


    @Setter
    public static class Order {
        private String id;
        private String value;
        private String date;

        public String getField(String name) {
            return switch (name) {
                case "id" -> id;
                case "value" -> value;
                case "date" -> date;
                default -> null;
            };
        }
    }

    @Getter@Setter
    public static class Filter {
        private Id id;
        private Value value;
    }

    @Getter@Setter
    public static class Id {
        private long eq;
        private long lt;
        private long gt;
        private Set<Long> in;
    }

    @Getter@Setter
    public static class Value {
        private String eq;
        private String ctn;
    }
}
