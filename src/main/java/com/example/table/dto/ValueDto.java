package com.example.table.dto;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class ValueDto {
    private String value;
}
