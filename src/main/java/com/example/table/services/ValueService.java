package com.example.table.services;

import com.example.table.dto.Params;
import com.example.table.entities.Value;
import com.example.table.repos.ValueRepository;
import com.example.table.util.FilterAndOrder;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class ValueService {
    private static String pattern = "Запись ";
    private final ValueRepository repository;

    public ValueService(ValueRepository repository) {
        this.repository = repository;
    }

    public int createListValues(int count) {
        LocalDateTime date = LocalDateTime.now();
        List<Value> values = IntStream.iterate(1, i -> i + 1).limit(count)
                .mapToObj(i -> new Value(date, pattern + i)).collect(Collectors.toList());
        return repository.saveAll(values).size();
    }

    @Transactional
    public long deleteAllValues() {
        long count = repository.count();
        repository.deleteAllInBatch();
        return count;
    }

    @Transactional
    public int updateValue(Long id, String value) {
        Value oldValue = repository.findById(id).orElseThrow();
        oldValue.setValue(value);
        return 1;
    }

    public List<Value> readAllValues(Params params) {
        Specification<Value> spec = FilterAndOrder.getSpec(params);
        Sort sort = FilterAndOrder.getSort(params);
        return repository.findAll(spec, sort);
    }
}
