package com.example.table.controllers;

import com.example.table.dto.Params;
import com.example.table.dto.ValueDto;
import com.example.table.entities.Value;
import com.example.table.services.ValueService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/values")
public class ValueController {
    private final ValueService service;

    public ValueController(ValueService service) {
        this.service = service;
    }

    @GetMapping
    public List<Value> getValues(Params params) {
        return service.readAllValues(params);
    }

    @PutMapping("/{id}")
    public int updateValue(@PathVariable long id, @RequestBody ValueDto dto) {
        return service.updateValue(id, dto.getValue());
    }

    @PostMapping
    public int createValues(@RequestParam int number) {
        return service.createListValues(number);
    }

    @DeleteMapping
    public long deleteValues() {
        return service.deleteAllValues();
    }
}
