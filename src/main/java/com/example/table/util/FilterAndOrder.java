package com.example.table.util;

import com.example.table.dto.Params;
import com.example.table.entities.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class FilterAndOrder {

    public static Sort getSort(@Nullable Params params) {
        if (params != null && params.getOrder() != null) {
            Params.Order order = params.getOrder();
            List<Sort.Order> list = new ArrayList<>();
            List.of("id", "value", "date").forEach(field -> {
                String string = order.getField(field);
                if (StringUtils.hasLength(string)) {
                    if (string.equals("asc")) {
                        list.add(Sort.Order.asc(field));
                    } else if (string.equals("desc")) {
                        list.add(Sort.Order.desc(field));
                    }
                }
            });
            if (list.size() > 0) return Sort.by(list);
        }
        return Sort.unsorted();
    }

    public static Specification<Value> getSpec(@Nullable Params params) {
        return Specification.where(getFilterById(params)).and(getFilterByValue(params));
    }

    private static Specification<Value> getFilterById(@Nullable Params params) {
        if (params != null && params.getFilter() != null && params.getFilter().getId() != null) {
            Params.Id id = params.getFilter().getId();
            if (id.getEq() > 0) {
                return (root, cq, cb) -> cb.equal(root.get("id"), id.getEq());
            }

            if (id.getIn() != null && !id.getIn().isEmpty()) {
                return (root, cq, cb) -> cb.in(root.get("id")).value(id.getIn());
            }

            if (id.getLt() > 0) {
                return (root, cq, cb) -> cb.lessThan(root.get("id"), id.getLt());
            }

            if (id.getGt() > 0) {
                return (root, cq, cb) -> cb.greaterThan(root.get("id"), id.getGt());
            }
        }
        return null;
    }

    private static Specification<Value> getFilterByValue(@Nullable Params params) {
        if (params != null && params.getFilter() != null && params.getFilter().getValue() != null) {
            Params.Value value = params.getFilter().getValue();
            if (StringUtils.hasLength(value.getEq())) {
                return (root, cq, cb) -> cb.equal(root.get("value"), value.getEq());
            }

            if (StringUtils.hasLength(value.getCtn())) {
                return (root, cq, cb) -> cb.like(root.get("value"), "%"+value.getCtn()+"%");
            }
        }
        return null;
    }
}
