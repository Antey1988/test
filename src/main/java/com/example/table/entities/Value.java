package com.example.table.entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.servlet.LocaleResolver;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "TableValues")
public class Value {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime date;

    private String value;

    public Value(LocalDateTime date, String value) {
        this.date = date;
        this.value = value;
    }

    public Value() {

    }
}
