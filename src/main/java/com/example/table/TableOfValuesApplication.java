package com.example.table;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TableOfValuesApplication {

    public static void main(String[] args) {
        SpringApplication.run(TableOfValuesApplication.class, args);
    }

}
